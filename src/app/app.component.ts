import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ContentfulService } from './contentful.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  blog$: Observable<any>;

 constructor(private contentful: ContentfulService){ }

 ngOnInit(){
     this.contentful.logContent('7BuKh64136dGVu5aMN9qzo')
 }

}
