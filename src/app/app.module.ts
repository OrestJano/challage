import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { ContentfulService } from './contentful.service';
import { BlogArticleComponent } from './blog-article/blog-article.component';

const routes: Routes = [
  { path: '', redirectTo: '/blogs', pathMatch: 'full'},
  { path: 'blogs', component: BlogListComponent },
  { path: 'blog/:id', component: BlogArticleComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    BlogListComponent,
    BlogArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ContentfulService],
  bootstrap: [AppComponent]
})
export class AppModule { }
