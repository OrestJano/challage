import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ContentfulService } from '../contentful.service';
import { Entry } from 'contentful';

@Component({
  selector: 'app-blog-article',
  templateUrl: './blog-article.component.html',
  styleUrls: ['./blog-article.component.css']
})
export class BlogArticleComponent implements OnInit {

  blog: Entry<any>;

  constructor( 
    private route: ActivatedRoute,
    private router: Router,
    private contentfulService: ContentfulService
    ) { }

  ngOnInit(){
    const contentId = this.route.snapshot.paramMap.get('id');
    this.contentfulService.getContent(contentId)
    .then((blog) => {
      this. blog = blog;
    } );
  }

  goToList(){
    this.router.navigate(['/blogs']);
  }

}
