import { Injectable } from '@angular/core';
import * as contentful from 'contentful';
import { createClient, Entry } from 'contentful';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ContentfulService {

  private client = createClient({
    space: environment.contentful.spaceId,
    accessToken: environment.contentful.token
  });

  // private client = contentful.createClient({
  //   space: environment.contentful.spaceId,
  //   accessToken: environment.contentful.token
  // })

  constructor() { }


  logContent(contentId){
    this.client.getEntry(contentId)
      .then((entry) => console.log(entry))
  }

  getContents(query?: object): Promise<Entry<any>[]>{
    return this.client.getEntries(Object.assign({
      content_type: 'blog'
    }, query))
    .then(res => res.items );
  }

  getContent(contentId): Promise<Entry<any>>{
    return this.client.getEntries(Object.assign({
      content_type: 'blog'
    }, {'sys.id': contentId}))
    .then(res => res.items[0]);
  }
  
}
